﻿using System.Collections;
using System.Collections.Generic;
using CardFrameworkCore;
using CardFrameworkUI;
using CardFrameworkExamples;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.TestTools;
using System.Linq;

namespace CardFrameworkTests
{
    public class GraphicalTests
    {
        Card cardExample;
        CardTemplate templateExample;
        GameObject cardObject;
        GameObject cardUICanvas;
        CardUI cardUIHandler;
        GameObject playAreaTest;

        [SetUp]
        public void Setup()
        {
            //   SceneManager.LoadScene("TestScene");

            GameObject cam = new GameObject();
            cam.AddComponent<Camera>();

            templateExample = Resources.Load<CardTemplate>("TestResources/CardExample");

            // var refToInst = Resources.Load<GameObject>("CardUICanvas");
            cardUICanvas = new GameObject();
            cardUICanvas.AddComponent<Canvas>();

            playAreaTest = new GameObject();
            playAreaTest.AddComponent<PlayAreaUI>();
            playAreaTest.GetComponent<PlayAreaUI>().playAreaSlot = Resources.Load<GameObject>("TestResources/Slot");

            var refToInst = Resources.Load<GameObject>("TestResources/CardObjectBase");
            cardObject = GameObject.Instantiate(refToInst,cardUICanvas.transform);

            cardExample = new Card(templateExample);
            cardUIHandler = cardObject.GetComponent<CardUI>();
            cardUIHandler.SetCardData(cardExample);

        //    cardUIHandler.SetCardData(cardExample);
            Debug.Log("Load");
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator GetCardDescriptionUponClickTest()//will test the function called upon click
        {

            ClickController newClickControl = new ClickController();
          
           
            yield return null;

            Assert.AreEqual(newClickControl.OnClickInteraction(cardObject), cardExample);

        }
        [UnityTest]
        public IEnumerator CardDroppedOnPlayAreaUITest()
        {
            SceneManager.LoadScene("TestScene");
            yield return new WaitForSeconds(1);
            Vector3 positionToCompare;

            templateExample = Resources.Load<CardTemplate>("TestResources/CardExample");
            cardExample = new Card(templateExample);

            cardObject = GameObject.FindGameObjectWithTag("TestCard");
            cardObject.GetComponent<CardUI>().SetCardData(cardExample);
            positionToCompare = cardObject.transform.position;

            playAreaTest = GameObject.FindGameObjectWithTag("TestPlayArea");
            playAreaTest.GetComponent<PlayAreaUI>().playAreaSlot = Resources.Load<GameObject>("TestResources/Slot");
            playAreaTest.GetComponent<PlayAreaUI>().SetupPlayAreaUI(new PlayArea(5));

            RecieveDropController newDropController = new RecieveDropController();
            newDropController.OnDropHandler(cardObject,playAreaTest);
            
            yield return new WaitForSeconds(4);
            Assert.AreNotEqual(cardObject.transform.position, positionToCompare);
           // Assert.IsTrue(false);//test passes but the behavior is not correct, go look at playareaui and look at the TODO
        }

      
        [UnityTest]
        public IEnumerator PlayAreaUISetUp()
        {           
            yield return new WaitForSeconds(1);
            GameObject playAreaUIOBJ = GameObject.Instantiate(new GameObject(), cardUICanvas.transform);
            PlayAreaUI uiRef = playAreaUIOBJ.AddComponent<PlayAreaUI>();
            

            uiRef.playAreaSlot = Resources.Load<GameObject>("TestResources/Slot");
            uiRef.SetupPlayAreaUI(new PlayArea(6));

            yield return new WaitForSeconds(1);

            Assert.AreEqual(playAreaUIOBJ.transform.childCount,6);


        }

        [UnityTest]
        public IEnumerator CardAddedToPlayAreaUISlotTest()
        {
            //test how moving a playarea card from one are to another functions
            yield return new WaitForSeconds(1);

            List<CardUI> cardsAdded = new List<CardUI>();
            PlayAreaUI testAreaUI = playAreaTest.GetComponent<PlayAreaUI>();
            testAreaUI.playAreaSlot = Resources.Load<GameObject>("Slot");
            yield return new WaitForSeconds(1);

            testAreaUI.playAreaSlot = Resources.Load<GameObject>("TestResources/Slot");
            testAreaUI.SetupPlayAreaUI(new PlayArea(6));

            yield return new WaitForSeconds(1);

            testAreaUI.AddCardToPlayArea(cardUIHandler);
            cardsAdded = testAreaUI.GetCardsInArea();

            int nonNullElementsInCardsAdded = cardsAdded.Count(s => s != null);

            Assert.AreEqual(1, nonNullElementsInCardsAdded);
            Assert.AreEqual(cardUIHandler, cardsAdded[0]);

        }



    }
}
