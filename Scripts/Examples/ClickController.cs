﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardFrameworkCore;
using CardFrameworkUI;
using System;

namespace CardFrameworkExamples
{
    public class ClickController : IClickHandler 
    {
        public Card OnClickInteraction(GameObject objectClicked)
        {
            CardUI cardUIRef = objectClicked.GetComponent<CardUI>();
            if (cardUIRef == null) {
                throw new Exception("There is no CardUI Attached ot the object");
            }
            return cardUIRef.GetCard();
        }
              
    }
}