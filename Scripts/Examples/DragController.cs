﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CardFrameworkCore;
using CardFrameworkUI;

namespace CardFrameworkExamples
{
    public class DragController : IDragAndDropHandler
    {

        public virtual void OnDragBeginHandler(GameObject objectBeingDragged)
        {
            objectBeingDragged.GetComponent<CanvasGroup>().blocksRaycasts = false;
        }

        public virtual void OnDragHandler(GameObject objectBeingDragged, Vector3 positionBeingDraggedTo)
        {
            objectBeingDragged.transform.position = positionBeingDraggedTo;
        }
               
        public virtual void OnEndDragHandler(GameObject objectBeingDragged,Vector3 resetPosition, bool shouldReset)
        {
            if (shouldReset == true) {
                objectBeingDragged.transform.localPosition = resetPosition;
            }

            objectBeingDragged.GetComponent<CanvasGroup>().blocksRaycasts = true;
        }
    }
}
