﻿using CardFrameworkCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using CardFrameworkUI;
using UnityEngine.UI;
using System;
using System.Linq;

namespace CardFrameworkExamples
{
    public class PlayAreaUI : MonoBehaviour, IDropHandler
    {
        //TODO: YOU SHOULD DO SOMETHING ABOUT THIS VARIABLE BEING NULL
        public GameObject playAreaSlot;


        public List<GameObject> slots;

        protected IDropIntoAreaInterface dropControl;
        protected PlayArea playAreaData;
        protected List<CardUI> cardsUIAdded;

        // Start is called before the first frame update
        void Start()
        {
          //  SetupPlayAreaUI(new PlayArea(2));
        }

        // Update is called once per frame
        void Update()
        {

        }

        public virtual void SetupPlayAreaUI(PlayArea newPlayAreaData, IDropIntoAreaInterface newDropControl=null) {

            cardsUIAdded = new List<CardUI>();
            playAreaData = newPlayAreaData;
            if (newDropControl == null)
            {
                dropControl = new RecieveDropController();
            }
            else {
                dropControl = newDropControl;
            }

          //  slots = new List<GameObject>();
            /*  gameObject.AddComponent<GridLayoutGroup>();
              GridLayoutGroup layoutRef = gameObject.GetComponent<GridLayoutGroup>();
              layoutRef.constraint = GridLayoutGroup.Constraint.FixedRowCount;

              if (playAreaSlot != null) {
                  for (int i = 0; i < newPlayAreaData.GetAmountIfElementsPlayable(); i++)
                  {
                      GameObject reference = Instantiate(playAreaSlot, transform);
                      slots.Add(reference);
                  }
              }*/

            cardsUIAdded.Capacity = newPlayAreaData.GetAmountIfElementsPlayable();

        }

        public virtual void AddCardToPlayArea(CardUI newVisualToAdd)
        {
            int nonNullElements = cardsUIAdded.Count(s => s != null);

            if (nonNullElements >=  slots.Count) {
                return;
            }

           
            cardsUIAdded.Add(newVisualToAdd);
            newVisualToAdd.gameObject.GetComponent<CanvasGroup>().blocksRaycasts = false;

            newVisualToAdd.transform.SetParent(slots[nonNullElements].gameObject.transform);
            
         
/* */
            slots[nonNullElements].GetComponent<Image>().enabled = false;

            //newVisualToAdd.transform.SetSiblingIndex(nonNullElements);

            StartCoroutine(SetPosition(newVisualToAdd));


        }


        IEnumerator SetPosition(CardUI newVisualToAdd)
        {

            yield return new WaitForEndOfFrame();
            newVisualToAdd.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
            newVisualToAdd.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0.5f);
            newVisualToAdd.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0.5f);
            newVisualToAdd.GetComponent<RectTransform>().localPosition = new Vector2(0f, 0);//slots[nonNullElements].GetComponent<RectTransform>().localPosition;

        }

        public virtual void ResetCardsInArea() {
            //very inneficient, should fix. 
            foreach (CardUI card in cardsUIAdded) {
                Destroy(card.gameObject);
            }
            foreach (GameObject slot in slots) {
                slot.GetComponent<Image>().enabled =true;
            }
            cardsUIAdded.Clear();
            gameObject.SetActive(false);
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (playAreaData.GetActiveStatus() == false) {
                return;
            }
           dropControl.OnDropHandler(eventData.pointerDrag,gameObject);
           Debug.Log("you just dropped "+eventData.pointerDrag.name);
           
        }

        public List<CardUI> GetCardsInArea()
        {
            return cardsUIAdded;
        }

        public PlayArea GetPlayAreaData()
        {

            return playAreaData;

        }
    }
}